###################################################################
#Selecting samples according to specific sampling designs
#  design 1: (SRSWOR; N, n)
#  design 2: (SRSWOR, STRATUM, Nh, nh)
#  design 3: (two-stage, (SRSWOR, SRSWOR, STRAT1, STR2, Nh, nh,))
#  design 4: two-phase, SRSWOR, SRSWOR, STR1, STR2, N1h, n1h
##################################################################

# Purpose: About sample selection using R
# required packages:
# survey; sampling; dplyr
set.seed(123)
root_path <- "C:/Users/chris/OneDrive/all_repositories/targeted_tutorials_r"
source(file.path(root_path, "helpers/1_helpers_functions.R"))

download_necessary_packages(c("survey", "dplyr", "sampling"))
data(api)

###
pop_data  <- tibble::as_tibble(apipop)
str(pop_data)
if ( length(unique(pop_data$cds)) == nrow(pop_data)){
  cat("No duplicate on cds")
}else{
  cat("cds is not unique")
}

list_variables <- c("cds", "stype", "snum", "dnum","cname", "cnum", "flag", "api00", "api99", "awards", "meals","api.stu")

sampling_frame <- pop_data[, list_variables]

### design 1: (SRSWOR; N, n)

download_necessary_packages("sampling")

sampling_frame <- sampling_frame[order(sampling_frame$stype),]


allocation <- floor( 0.15* nrow(sampling_frame)    )

strata_obj <- sampling::strata(data = sampling_frame,
                 stratanames = NULL, 
                 size = allocation,
                 method = c("srswor")
)

str(strata_obj) # > object of class dataframe
strata_obj <- tibble::as_tibble(strata_obj)
strata_obj

## extracting the sampled data
sample_records <- sampling::getdata(data = sampling_frame, m = strata_obj)
sample_records <- tibble::as_tibble(sample_records)
sample_records


### design 2: (SRSWOR, STRATUM, Nh, nh)
rm(list= ls())
sampling_frame <- sampling_frame[order(sampling_frame$stype),]

## find the allocation distribution;
temp <- table(sampling_frame$stype, useNA = c("ifany"))
temp
temp <- data.frame(temp)
colnames(temp) <- c("stype", "capNh")

temp$ssizeh <- floor( 0.15*temp$capNh) # determine sample size per stratum

allocation_d2 <- temp
rm(list = c("temp"))

strata_obj <- sampling::strata(data = sampling_frame, 
                               stratanames = c("stype"),
                               size = allocation_d2$ssizeh,
                               method = c("srswor")
                               )

strata_obj <- tibble::as_tibble(strata_obj)
## getting data
sample_records_d2 <- sampling::getdata(data = sampling_frame, m = strata_obj)
sample_records_d2 <- tibble::as_tibble(sample_records_d2)

sample_records_d2

### design 3: (two-stage, (SRSWOR, SRSWOR, STRAT1, STR2, Nh, nh,))
# stype: stratification variable - Three strata (E - 669, H - 355, M - 445)
# dnum: district number - Primary Sampling Unit (PSU) [669*0.05, 355*0.05, 445*0.05] ===[33,17,22]
# snum: school number - Secondary Sampling Unit (SSU)[take a single element in each selected PSU]

rm(list=ls())

# order by the stratification variable
sampling_frame <- sampling_frame[order(sampling_frame$stype),]

# distribution of number schools per school type and district areas
dist_school <- sampling_frame %>% group_by(stype, dnum) %>% summarise(nbSchool = n())
dist_school
# number of schools per school type (stratum)
allocation <- sampling_frame %>% group_by(stype) %>% summarise(capNh = n())


# specify the number of districts to select for each school type - 5% of the districts within the stratum
numberOfDistricts <- data.frame(  u = table(dist_school$stype) )
colnames(numberOfDistricts) <- c("stype", "nbDistricts")
numberOfDistricts
numberOfDistricts$ssizeh1 <- floor( 0.05*numberOfDistricts$nbDistricts )

allocation
allocation_d3 <- merge(allocation, numberOfDistricts, by = c("stype"))
allocation_d3

size_all = list(
  size1 = allocation_d3$capNh,
  size2 = allocation_d3$ssizeh1, ## number of districts to randomly pick within stratum
  size3 = rep(1, sum(allocation_d3$ssizeh1)) # select only one school per selected district.
)
size_all
stage_obj <- sampling::mstage(data = sampling_frame, 
                 stage = c("stratified", "cluster", "cluster"), 
                 varnames = list("stype", "dnum", "snum"),
                 size = size_all,
                 method = c("","srswor","srswor")
)

# extract the second stage sample;
selected_sample_2stage <- getdata(data = sampling_frame, m = stage_obj)[[3]]
# convert to tibble
selected_sample_2stage <- tibble::as_tibble(selected_sample_2stage)
selected_sample_2stage

sum(1/selected_sample_2stage$`Prob_ 3 _stage`)

#### design 4: two-phase, SRSWOR, SRSWOR, STR1, STR2, N1h, n1h, 

sampling_frame <- sampling_frame[order(sampling_frame$stype),]

size_all <- list(
  size1 <- {
    table(sampling_frame$stype)
  },
  
  size2 <- {  floor( as.data.frame( table(sampling_frame$stype))$Freq*0.15  )
    }
)

stage_1 <- sampling::mstage(data = sampling_frame, 
                            stage = c("stratified", "cluster"),
                            varnames = c("stype", "snum"),
                            size = size_all,
                            method = c("", "srswor")
                            )

stage_1

## the first stage is: stage_1[[1]]

pop_stra <- getdata(data = sampling_frame, m = stage_1)[[1]]
pop_stra <- tibble::as_tibble(pop_stra)
pop_stra
##> all the prob = 1 as expected - and the number of row is 6194 as expected;

# observed data

first_phase_sample <- getdata(sampling_frame, stage_1)[[2]]

# rename the inclusion variable's name and the remove the ID_unit
first_phase_sample2 <- first_phase_sample %>% mutate(inclu_phase1 = `Prob_ 2 _stage`)
first_phase_sample2 <- first_phase_sample2[, !(colnames(first_phase_sample2) %in% c("Prob_ 2 _stage", "Prob"))]
first_phase_sample2
### now turn on to the second phase


size_all2 <- list(
  size1 <- table(first_phase_sample2$stype),
  size2 <- {  floor( as.data.frame( table(first_phase_sample2$stype))$Freq*0.25  )
  }
)
size_all2

stage_2 <- sampling::mstage(data = first_phase_sample2, 
                            stage = c("stratified", "cluster"),
                            varnames = c("stype", "snum"),
                            size = size_all2,
                            method = c("", "srswor")
)

second_phase_sample <- getdata(first_phase_sample2, stage_2)[[2]]

# rename the inclusion variable's name and then remove the ID_unit variable
second_phase_sample2 <- second_phase_sample %>% mutate(inclu_phase2 = `Prob_ 2 _stage`)
second_phase_sample2

dim(second_phase_sample2)

second_phase_sample2 <- tibble::as_tibble(second_phase_sample2)
View(second_phase_sample2)



### Homeworks

# hmw1: Use our made up population to select a random sample according to design 2. Use regions as
#       your stratification variable. Select in each stratum a sample using SRSWOR with a sampling
#       fraction of 15%.

# hmw2: Investigate if design 4 can be coded using only one call to the mstage function instead of two
#       

# hmw3: Using design 2 as a starting point, create a stratify one-stage design using the same sampling
#       frame. The clusters are defined by the district number (dnum) and the stratification variable
#       is still stype. [For hints on how to select a cluster design, you can refer to design 3].Use the
#       same sampling fraction of 15% as well.

# hmw4: Create a database using SQLITE engine and save all the four samples selected in this tutorial
#       as well as a copy of the sampling_frame. Give a name to your database: "samplingDataBase". The
#       different samples should be save under the following names: sample_records_d1, sample_records_d2,
#       sample_records_d3 and sample_records_d4 respectively.
#
#



