############
# purpose: calibration weights
# 
# 
#  Any weighting system is function that takes as input
#  a set of weights and returned a transform set of weights.
# 
#  In this tutorial - we explore how to do calibration using
#  the package "survey"
#
#
# Design 1: SRSWOR - sampling fraction 15%
# Design 2: SRSWOR - STRATIFIED (stype) - sampling fraction 15% in each stratum
#
#################################

# specify the working directory;
root_path <- "C:/Users/chris/OneDrive/all_repositories/targeted_tutorials_r"
source(file.path(root_path, "helpers/1_helpers_functions.R"))

# install and load required packages
download_necessary_packages(c("survey", "DBI", "dplyr"))

###########

# connect to our local database
con <- DBI::dbConnect(RSQLite::SQLite(), file.path(root_path,"/data/samplingDB"))
dbListTables(con)

sampling_frame <- DBI::dbGetQuery(con, "select * from sampling_frame")


## design 1:

survey_data <- DBI::dbGetQuery(con, "select * from sample_d1")
head(survey_data)
survey_data$weight <- 1/survey_data$Prob

survey_data$fpc <- survey_data$weight*nrow(survey_data)

nrow(survey_data)


# specify the sampling design
design1 <- survey::svydesign(ids = ~snum,
                             strata = NULL,
                             data = survey_data,
                             fpc = ~fpc)

# request a summary of the sampling design
summary(design1)


## calibration can help improved this estimators.
## let say you are interested in calibration to some totals known 
## in the sampling frame such the total api99 by awards

control.totals <- sampling_frame %>% group_by(awards) %>% summarise(api99_awards = sum(api99))
control.totals

# using the raking distance function; force the weights to be positive;
design1_cal <- survey::calibrate(design = design1,
                            formula = ~ -1 + meals +api99,
                            population = c( sum(sampling_frame$meals),sum(sampling_frame$api99)),
                            calfun = c("linear"))


weights(design1_cal)

u <- weights(design1_cal)
summary(u)

g_factor1 <- u/(1/design1$prob)

hist(g_factor1)


# produce the mean of api00 by stype and awards
survey::svyby(~api00, by = ~stype:awards, design =design1, svymean) # this is domain estimation.
survey::svyby(~api00, by = ~stype:awards, design =design1_cal, svymean) # this is domain estimation.


# design 2
survey_data <- DBI::dbGetQuery(con, "select * from sample_d2")
survey_data$weight <- 1/survey_data$Prob

allocation_d2 <- DBI::dbGetQuery(con, "select * from allocation_d2")
allocation_d2

survey_data <- survey_data %>% inner_join(allocation_d2, by = "stype")

design2<-svydesign(id=~snum, 
                  strata = ~ stype,
                  weights=~weight,
                  data=survey_data,
                  fpc=~capNh)

summary(design2)

# calculate control totals
pop_tot <- sampling_frame %>% summarise(api99 = sum(api99))
pop_tot

design2_cal<-calibrate(design = design2,
                   formula = ~ -1+api99, 
                   population = pop_tot$api99,
                   calfun = c("raking"))


w<- 1/design2$prob

u <- weights(design2_cal)
summary(u/w)

g_factor <- u/w

hist(g_factor)


svyby(make.formula(c("api00", "api99", "meals")),
      by = ~stype, 
      design = design2,
      svymean)

svyby(make.formula(c("api00", "api99")),
      by = ~stype, 
      design = design2_cal,
      svymean)




### homeworks

#hmw 1: Perform calibration using design 3. Some collapsing of the groups
#    might be required. Choose your control totals.


# hmw 2: Perform calibration using design 4. Choose your auxiliary variables
# for calibration.


## hmw 3: Add our made up population to our database: both at household 
# and person level.


## hmw 4: Using the household file to perform calibration using the survey
# package. First select a sample with a sampling fraction of 5%. Choose
# your variable of interest and auxiliary variable(s) to which you would
# like to calibrate on





